package com.vincentvbs.topquiz.model;

import java.util.List;

public class QuestionBank {
    public QuestionBank(List<Question> questionList) {
        // todo : shuffle question list before storing it
        mQuestionList = questionList;
        mCurrentQuestionIndex = 0;
    }

    public Question getCurrentQuestion() {
        Question retQuestion = mQuestionList.get(mCurrentQuestionIndex);
        return retQuestion;
    }
    public Question getNextQuestion() {
        mCurrentQuestionIndex++;
        mCurrentQuestionIndex %=mQuestionList.size();
        return getCurrentQuestion();
    }

    private List<Question> mQuestionList;
    private int mCurrentQuestionIndex;
}
