package com.vincentvbs.topquiz.controller;

interface MainView {
    void showPlayButton(boolean show);

    void startGameActivity();

    void setGreetingText(CharSequence s);
}
