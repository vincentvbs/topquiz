package com.vincentvbs.topquiz.controller;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.vincentvbs.topquiz.R;
import com.vincentvbs.topquiz.model.Question;

import java.util.List;

public class GameActivity extends AppCompatActivity implements GameView, View.OnClickListener {

    private TextView mQuestionText;
    private Button mAnswer;
    private Button mAnswer2;
    private Button mAnswer3;
    private Button mAnswer4;
    private GamePresenter mGamePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mQuestionText = (TextView) findViewById(R.id.activity_game_textView);
        mAnswer = (Button) findViewById(R.id.activity_game_button);
        mAnswer2 = (Button) findViewById(R.id.activity_game_button2);
        mAnswer3 = (Button) findViewById(R.id.activity_game_button3);
        mAnswer4 = (Button) findViewById(R.id.activity_game_button4);

        mGamePresenter = new GamePresenter(this);


        // Use the same listener for the four buttons.
        // The tag value will be used to distinguish the button triggered
        mAnswer.setOnClickListener(this);
        mAnswer2.setOnClickListener(this);
        mAnswer3.setOnClickListener(this);
        mAnswer4.setOnClickListener(this);

        // Use the tag property to 'name' the buttons
        mAnswer.setTag(0);
        mAnswer2.setTag(1);
        mAnswer3.setTag(2);
        mAnswer4.setTag(3);
    }


    public void onClick(View v) {
        int userAnswerIndex = (int) v.getTag();
        mGamePresenter.answerPushed(userAnswerIndex);

    }

    @Override
    public void displayQuestion(final Question question) {
        // Set the text for the question text view and the four buttons
        mQuestionText.setText(question.getQuestion());
        List<String> answers = question.getChoiceList();
        assert answers.size() != 4;
        mAnswer.setText(answers.get(0));
        mAnswer2.setText(answers.get(1));
        mAnswer3.setText(answers.get(2));
        mAnswer4.setText(answers.get(3));
    }



    @Override
    public void displayResultGood(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayResultWrong(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayScore(String comment, String score, String button_text) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(comment)
                .setMessage(score)
                .setPositiveButton(button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();
    }
}
