package com.vincentvbs.topquiz.controller;

import com.vincentvbs.topquiz.model.Question;
import com.vincentvbs.topquiz.model.QuestionBank;

import java.util.Arrays;

public class GamePresenter {
    private GameView mGameView;
    private QuestionBank mQuestionBank;
    private int mMaxNumberOfQuestions;
    private int mAskedQuestions;
    private int mScore;

    public GamePresenter(GameView gameView) {
        mGameView = gameView;


        mQuestionBank = this.generateQuestions();
        mGameView.displayQuestion(mQuestionBank.getCurrentQuestion());
        mMaxNumberOfQuestions = 3;
        mAskedQuestions = 0;
        mScore = 0;
    }

    public void answerPushed(int userAnswerIndex) {

        int correctAnswerIndex = mQuestionBank.getCurrentQuestion().getAnswerIndex();
        if(userAnswerIndex == correctAnswerIndex) {
            mGameView.displayResultGood("Correct!");
            mScore++;
        }
        else {
            mGameView.displayResultWrong("Incorrect. Answer was '"+mQuestionBank.getCurrentQuestion().getChoiceList().get(correctAnswerIndex)+"'");

        }
        mAskedQuestions++;
        if(mAskedQuestions<mMaxNumberOfQuestions)
            mGameView.displayQuestion(mQuestionBank.getNextQuestion());
        else {
            //Toast.makeText(this, "Finish! Score is "+mScore+"/"+mMaxNumberOfQuestions, Toast.LENGTH_LONG).show();
            mGameView.displayScore("Well done!","Your score is " + mScore,"OK");
        }
    }

    protected QuestionBank generateQuestions() {
        Question question1 = new Question("Who is the creator of Android?",
                Arrays.asList("Andy Rubin",
                        "Steve Wozniak",
                        "Jake Wharton",
                        "Paul Smith"),
                0);

        Question question2 = new Question("When did the first man land on the moon?",
                Arrays.asList("1958",
                        "1962",
                        "1967",
                        "1969"),
                3);

        Question question3 = new Question("What is the house number of The Simpsons?",
                Arrays.asList("42",
                        "101",
                        "666",
                        "742"),
                3);

        return new QuestionBank(Arrays.asList(question1, question2, question3));
    }
}
