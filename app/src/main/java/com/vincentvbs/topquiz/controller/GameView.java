package com.vincentvbs.topquiz.controller;

import com.vincentvbs.topquiz.model.Question;

interface GameView {

    void displayResultGood(String s);
    void displayResultWrong(String s);

    void displayScore(String comment, String score, String button_text);

    void displayQuestion(Question nextQuestion);
}
